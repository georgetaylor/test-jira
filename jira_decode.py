import os
import json
import sys

s = sys.stdout

jira_payload_path =  os.environ['TRIGGER_PAYLOAD']

with open(jira_payload_path) as file:
    jira_payload = json.load(file)

    requestType = jira_payload.get('fields').get('customfield_10001').get('requestType').get('name')

    storage_size = jira_payload.get('fields').get('customfield_11501').get('value')

    s.write('Request Type: ' + requestType +'\n')
    s.write('Options: \n' + 'Storage size: ' + storage_size +'\n')
